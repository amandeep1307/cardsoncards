/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package cards;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

/**
 *
 * @author Mann
 */
public class Cards extends JFrame {
    
    private int HEARTS=1;
    private int SPADES=2;
    private int DIAMONDS=3;
    private int CLUBS=4;
    private int ACE=1;
    private int JACK=11;
    private int QUEEN=12;
    private int KING=13;
    
    List <data> data1=new ArrayList <data>();
    List <data> player_1=new ArrayList <data>();
    List <data> player_2=new ArrayList <data>();
    JButton btn1;
    JButton btn2;
    JLabel label3;
    String path="";
    
    int PL1_counter=0;
    int PL2_counter=0;
    int PL1_value=0;
    int PL2_value=0;

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Cards obj1=new Cards();
        obj1.setVisible(true);
        
        
        
        
        
        // TODO code application logic here
    }
    public Cards()
    {
       String suit="";
        
        for(int i=1;i<=4;i++)
        {
            for(int j=1;j<=13;j++)
            {
                
                switch(i)
                {
                    case 1:
                        suit="HEARTS";
                        break;
                    case 2:
                        suit="SPADES";
                        break;
                    case 3:
                        suit="DIAMONDS";
                        break;
                    case 4:
                        suit="CLUBS";
                        break;
                }
                data1.add(new data(j,suit,i));
                        
            }
        }
        
        
        Collections.shuffle(data1);
        
        
        player_1=data1.subList(0,25);
        player_2=data1.subList(26,51);
        
        
        /*System.out.println("Original");
        
        for(data d:data1)
        {
            System.out.println("Number is :- "+d.number+" Suit is :- "+d.suit);
        }
        
        System.out.println("1st List");
        
        for(data e:player_1)
        {
            System.out.println("Number is :- "+e.number+" Suit is :- "+e.suit);
        }
        
        System.out.println("2nd List");
        
        for(data f:player_2)
        {
            System.out.println("Number is :- "+f.number+" Suit is :- "+f.suit);
        }*/ 
        
        
       setTitle("Patte te Patta ! "); 
       
       int noRows=3;
       int noCols=1;
       
       JPanel pan=new JPanel(new BorderLayout());
       GridLayout b=new GridLayout(noRows,noCols);
       pan.setLayout(b);
       
       btn1=new JButton("Player 1. ");
       btn1.setToolTipText("Player 1. ");
       pan.add(btn1);
       btn1.addActionListener(new PL1());
       
       btn2=new JButton("Player 2. ");
       btn2.setToolTipText("Player 2. ");
       pan.add(btn2);
       btn2.addActionListener(new PL2());
       btn2.setEnabled(false);
       
//       ImageIcon icon = new ImageIcon("D:\\a.jpg");
//       label3 = new JLabel(icon);
       
       label3 = new JLabel(new ImageIcon("D:\\all_cards\\back.png"));
       pan.add(label3);
       
       getContentPane().add(pan);
       pack();
       
       
       
    }

    
    class PL1 implements ActionListener
    {

        @Override
        public void actionPerformed(ActionEvent e) {
            //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        
        if(PL1_counter<player_1.size())
        {
        label3.setIcon(new ImageIcon("D:\\all_cards\\"+player_1.get(PL1_counter).number+"-"+player_1.get(PL1_counter).s_num+".png"));
        System.out.println("A'z turn ! "+"Number is :- "+player_1.get(PL1_counter).number+" Suit is :- "+player_1.get(PL1_counter).suit);
        
        btn1.setEnabled(false);
        btn2.setEnabled(true);        
              
        PL1_value=player_1.get(PL1_counter).number;
        PL1_counter++;
        }
        else
        {
            btn1.setEnabled(false);
            btn2.setEnabled(true);
        }
        if(PL1_value==PL2_value)
        {
            System.out.println(" A WON !");
            btn1.setEnabled(false);
            btn2.setEnabled(false);
            
        }
        
        }
        
    }
    
    
    class PL2 implements ActionListener
    {

        @Override
        public void actionPerformed(ActionEvent e) {
            //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        
        if(PL2_counter<player_2.size())
        { 
        label3.setIcon(new ImageIcon("D:\\all_cards\\"+player_2.get(PL2_counter).number+"-"+player_2.get(PL2_counter).s_num+".png"));    
        System.out.println("B'z turn ! "+"Number is :- "+player_2.get(PL2_counter).number+" Suit is :- "+player_2.get(PL2_counter).suit);
        
        btn2.setEnabled(false);
        btn1.setEnabled(true);
        
        PL2_value=player_2.get(PL2_counter).number;
        PL2_counter++;
        }
        else
        {
            btn2.setEnabled(false);
            System.out.println("NO ONE WON !");
        }
        
        if(PL2_value==PL1_value)
        {
            System.out.println(" B WON !");
            btn1.setEnabled(false);
            btn2.setEnabled(false);
            
        }
        
        }
        
        }
        
    }

